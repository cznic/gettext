# Gettext

Package gettext emulates libintl by doing nothing useful.

## Installation

    $ go get modernc.org/gettext

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/gettext/lib

## Documentation

[godoc.org/modernc.org/gettext](http://godoc.org/modernc.org/gettext)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fgettext](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fgettext)
